using UnityEngine;
using System.Collections;

public class audioclip_clone : MonoBehaviour {
	
	public AudioClip original_audio_clip;
	public AudioClip clone_audio_clip;
	public float[] clone_audio_clip_samples;
	int passage_samples_count;
	
	// Use this for initialization
	void Start () {
		clone_audio_clip = AudioClip.Create( "CLONE", original_audio_clip.samples, original_audio_clip.channels, original_audio_clip.frequency, false, true, OnAudioRead, OnSetPosition );
		audio.clip = clone_audio_clip;
		clone_audio_clip_samples = new float[ clone_audio_clip.samples * clone_audio_clip.channels ];
		original_audio_clip.GetData( clone_audio_clip_samples, 0 );
		audio.Play();
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	void OnAudioRead( float[] data ) {
		int count = 0;
		while ( count < data.Length ) {
			data[count] = clone_audio_clip_samples[passage_samples_count % clone_audio_clip_samples.Length];
			passage_samples_count++;
			count++;
		}
	}
	
	void OnSetPosition( int position )
	{
		passage_samples_count = position;
	}
}
